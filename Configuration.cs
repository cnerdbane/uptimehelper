﻿using Dalamud.Configuration;
using Dalamud.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;

namespace UptimeHelper
{
    public class Configuration : IPluginConfiguration {
        [NonSerialized]
        private DalamudPluginInterface pluginInterface;

        [NonSerialized]
        private UptimeHelper plugin;

        [NonSerialized]
        public Dictionary<uint, Dictionary<Guid, Notification>> NotificationLookup = new Dictionary<uint, Dictionary<Guid, Notification>>();

        int IPluginConfiguration.Version { get; set; }

        public int SoundDeviceId { get; set; } = -1;
        public bool PlayInBackground { get; set; } = true;
        public bool DebugChatText { get; set; } = false;
        public ObservableCollection<Notification> Notifications { get; set; } = new ObservableCollection<Notification>()
        {
            //new Notification { StatusId = 1871, AudioFilePath = "", Volume = 0.5f, OffsetSeconds = -4.0f }, // Dia
        };

        public void Init(UptimeHelper plugin, DalamudPluginInterface pluginInterface)
        {
            this.plugin = plugin;
            this.pluginInterface = pluginInterface;

            this.Notifications.CollectionChanged += this.Notifications_CollectionChanged;

            // call it once to initialize
            this.Notifications_CollectionChanged(
                this.Notifications,
                new NotifyCollectionChangedEventArgs(
                    NotifyCollectionChangedAction.Add,
                    this.Notifications
                )
            );
        }

		private void Notifications_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
                    foreach (Notification notification in e.NewItems)
                    {
                        if (!this.NotificationLookup.ContainsKey(notification.StatusId) || this.NotificationLookup[notification.StatusId] is null)
                        {
                            this.NotificationLookup[notification.StatusId] = new Dictionary<Guid, Notification>();
                        }

                        this.NotificationLookup[notification.StatusId][notification.Id] = notification;
                    }
                    break;

				case NotifyCollectionChangedAction.Remove:
                    foreach (Notification notification in e.OldItems)
                    {
                        if (this.NotificationLookup.ContainsKey(notification.StatusId))
                        {
                            this.NotificationLookup[notification.StatusId]?.Remove(notification.Id);
                        }
                    }
					break;
			}
		}

		public void Save()
        {
            pluginInterface.SavePluginConfig(this);
            plugin.InvalidateState();
        }
	}
}