﻿using System;
using System.Collections.Generic;
using Dalamud.Interface;
using ImGuiNET;

namespace UptimeHelper
{
	public class UiHelper
	{
		public static bool IconButton(FontAwesomeIcon icon, string id = null, string tooltip = null)
		{
			string label = icon.ToIconString();
			if (id is not null)
			{
				label += $"##{id}";
			}

			ImGui.PushFont(UiBuilder.IconFont);
			bool ret = ImGui.Button(label);
			ImGui.PopFont();

			if (tooltip != null && ImGui.IsItemHovered())
			{
				ImGui.BeginTooltip();
				ImGui.TextUnformatted(tooltip);
				ImGui.EndTooltip();
			}

			return ret;
		}

		internal static void SetupTableHeaders(string[] headers)
		{
			int columnCount = ImGui.TableGetColumnCount();

			for (int i = 0; i < headers.Length; i++)
			{
				ImGui.TableNextColumn();
				ImGui.TableHeader(headers[i]);
			}

			for (int i = headers.Length; i < columnCount; i++)
			{
				ImGui.TableNextColumn();
				ImGui.TableHeader(String.Empty);
			}
		}

		private static int dragging = -1;
		private static (int src, int dst)? drag = null;

		internal static void TableDragItem(string label, int rowIndex, string id = null)
		{
			if (id is not null)
			{
				label += $"##{id}";
			}
			ImGui.Selectable($"{label}");

			// handle dragging
			if (ImGui.IsItemActive() || dragging == rowIndex)
			{
				dragging = rowIndex;
				var step = 0;
				if (ImGui.GetIO().MouseDelta.Y < 0 && ImGui.GetMousePos().Y < ImGui.GetItemRectMin().Y)
				{
					step = -1;
				}

				if (ImGui.GetIO().MouseDelta.Y > 0 && ImGui.GetMousePos().Y > ImGui.GetItemRectMax().Y)
				{
					step = 1;
				}

				if (step != 0)
				{
					drag = (rowIndex, rowIndex + step);
				}
			}
		}

		internal static void SetupTableDragReordering<T>(IList<T> rowItems)
		{
			if (!ImGui.IsMouseDown(ImGuiMouseButton.Left) && dragging != -1)
			{
				dragging = -1;
			}

			if (drag != null && drag.Value.dst < rowItems.Count && drag.Value.dst >= 0)
			{
				dragging = drag.Value.dst;

				T temp = rowItems[drag.Value.src];
				rowItems[drag.Value.src] = rowItems[drag.Value.dst];
				rowItems[drag.Value.dst] = temp;
			}

			drag = null;
		}
	}
}
