﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dalamud.Interface.ImGuiFileDialog;
using Dalamud.Interface.Windowing;
using Dalamud.Utility;
using ImGuiNET;
using NAudio.Wave;
using Lumina.Excel.GeneratedSheets;
using Dalamud.Interface;
using System.Threading;
using System.Numerics;

namespace UptimeHelper
{
	public class PluginConfiguration : Window
	{
		public const string WINDOW_NAME = "Uptime Helper Configuration";
		private const int STATUS_TABLE_VISIBLE_ROWS = 8;

		private readonly FileDialogManager _dialogManager;
		private readonly Dictionary<uint, Status> statuses;
		private readonly Dictionary<Guid, CancellationTokenSource> playingNotifications;
		private string addNewStatusName = String.Empty;
		private uint addNewStatusId = 0;

		// Methods
		public PluginConfiguration() : base(WINDOW_NAME)
		{
			_dialogManager = SetupDialogManager();
			IsOpen = false;
			Size = new(810, 520);
			SizeCondition = ImGuiCond.FirstUseEver;

			this.statuses = new Dictionary<uint, Status>();
			this.playingNotifications = new Dictionary<Guid, CancellationTokenSource>();

			foreach (Status status in Service.DataManager.GetExcelSheet<Status>(Dalamud.ClientLanguage.English))
			{
				if (false == this.statuses.ContainsKey(status.RowId))
				{
					this.statuses[status.RowId] = status;
				}
			}
		}

		public override void Draw()
		{
			var deviceId = Service.Config.SoundDeviceId;
			var deviceName = "Default";
			if (deviceId > -1)
			{
				if (deviceId < WaveOut.DeviceCount)
				{
					deviceName = WaveOut.GetCapabilities(deviceId).ProductName;
				}
				else
				{
					Service.Config.SoundDeviceId = -1;
				}
			}
			else
			{
				Service.Config.SoundDeviceId = -1;
			}

			if (ImGui.BeginCombo("Sound Device", deviceName))
			{
				if (ImGui.Selectable("Default"))
				{
					Service.Config.SoundDeviceId = -1;
				}

				for (int i = 0; i < WaveOut.DeviceCount; i++)
				{
					if (ImGui.Selectable(WaveOut.GetCapabilities(i).ProductName))
					{
						Service.Config.SoundDeviceId = i;
					}
				}

				ImGui.EndCombo();
			}

			var playInBackground = Service.Config.PlayInBackground;
			if (ImGui.Checkbox("Play Sound in Background##PlayInBackground", ref playInBackground))
			{
				Service.Config.PlayInBackground = playInBackground;
			}
			var debugChatText = Service.Config.DebugChatText;
			if (ImGui.Checkbox("Debug Chat Text##DebugChatText", ref debugChatText))
			{
				Service.Config.DebugChatText = debugChatText;
			}

			ImGui.NewLine();
			ImGui.Separator();
			ImGui.NewLine();

			NotificationSoundTable();

			_dialogManager.Draw();

			ImGui.NewLine();
			ImGui.Separator();

			if (ImGui.Button("Close"))
			{
				SoundEngine.StopAllSounds();
				this.IsOpen = false;
			}
		}

		private void NotificationSoundTable()
		{
			ImGui.InputTextWithHint("##NewId", "Search...", ref addNewStatusName, 512, ImGuiInputTextFlags.AutoSelectAll);
			ImGui.SameLine();
			if (ImGui.Button("Add##AddNew"))
			{
				if (this.statuses.ContainsKey(addNewStatusId))
				{
					Service.Config.Notifications.Add(new Notification() { StatusId = addNewStatusId });
					addNewStatusName = String.Empty; // reset
					addNewStatusId = 0;
				}
			}

			if (ImGui.BeginCombo("##NewStatus", addNewStatusName))
			{
				foreach (Status status in this.statuses.Values
					.Where(s => s.Name.RawString.StartsWith(addNewStatusName, StringComparison.OrdinalIgnoreCase)))
				{
					if (ImGui.Selectable($"{status.RowId} : {status.Name}"))
					{
						addNewStatusName = status.Name;
						addNewStatusId = status.RowId;
					}
				}

				ImGui.EndCombo();
			}

			ImGui.NewLine();

			const ImGuiTableFlags flags = ImGuiTableFlags.None
				| ImGuiTableFlags.NoSavedSettings
				| ImGuiTableFlags.SizingStretchProp
				| ImGuiTableFlags.ScrollY
			;
			float TEXT_BASE_HEIGHT = ImGui.GetTextLineHeightWithSpacing();
			Vector2 outerSize = new Vector2(0.0f, TEXT_BASE_HEIGHT * STATUS_TABLE_VISIBLE_ROWS);

			if (!ImGui.BeginTable("Notifications", 9, flags, outerSize)) return;

			UiHelper.SetupTableHeaders(new[] {
				"Enable",
				"Status",
				"Offset (s)",
				"Chat",
				"File Path",
				"",
				"",
				"Volume",
			});

			UiHelper.SetupTableDragReordering(Service.Config.Notifications);

			for (int i = 0; i < Service.Config.Notifications.Count; i++)
			{
				Notification notification = Service.Config.Notifications[i];

				// Enable
				ImGui.TableNextColumn();
				bool enabled = notification.Enabled;
				if (ImGui.Checkbox($"##Enable{i}", ref enabled))
				{
					notification.Enabled = enabled;
				}

				// Status Name
				ImGui.TableNextColumn();
				UiHelper.TableDragItem($"{this.statuses[notification.StatusId].Name} ({notification.StatusId})", i, notification.Id.ToString());

				// Offset time
				ImGui.TableNextColumn();
				ImGui.SetNextItemWidth(100);
				string offsetSecondsString = notification.OffsetSeconds.ToString();
				if (ImGui.InputText($"##OffsetSeconds{i}", ref offsetSecondsString, 10, ImGuiInputTextFlags.CharsDecimal))
				{
					if (float.TryParse(offsetSecondsString, out float offsetSeconds))
					{
						notification.OffsetSeconds = offsetSeconds;
					}
				}

				// Enable Chat Log
				ImGui.TableNextColumn();
				bool showInChat = notification.ShowInChat;
				if (ImGui.Checkbox($"##Chat{i}", ref showInChat))
				{
					notification.ShowInChat = showInChat;
				}

				// Audio File Path
				ImGui.TableNextColumn();
				ImGui.SetNextItemWidth(this.Size.Value.X / 2.0f);
				string notificationPath = notification.AudioFilePath;
				ImGui.InputText($"##NotifyPath{i}", ref notificationPath, 512, ImGuiInputTextFlags.ReadOnly);

				// Browse button
				ImGui.TableNextColumn();
				if (UiHelper.IconButton(FontAwesomeIcon.FolderOpen, $"NotifyBrowse{i}", "Browse..."))
				{
					string startDir = Path.GetDirectoryName(notification.AudioFilePath);

					void UpdatePath(bool success, List<string> paths)
					{
						if (success && paths.Count > 0)
						{
							notification.AudioFilePath = paths[0];
						}
					}

					_dialogManager.OpenFileDialog("Choose an audio file", "Audio Files{.wav,.mp3}", UpdatePath, 1, startDir);
				}

				// Test audio button
				ImGui.TableNextColumn();
				if (this.IsPlaying(notification))
				{
					if (UiHelper.IconButton(FontAwesomeIcon.Stop, $"NotifyPath{i}", "Stop audio"))
					{
						this.StopNotification(notification);
					}
				}
				else
				{
					if (UiHelper.IconButton(FontAwesomeIcon.Play, $"NotifyPath{i}", "Test audio"))
					{
						this.PlayNotification(notification);
					}
				}

				// Volume Slider
				ImGui.TableNextColumn();
				ImGui.SetNextItemWidth(100);
				float volume = notification.Volume * 100f;
				if (ImGui.SliderFloat($"##Volume{i}", ref volume, 0, 100.0f, "%.1f%%"))
				{
					notification.Volume = Math.Min(volume / 100f, 1); // Todo: this may not save back if the reference is broken
				}

				// Remove button
				ImGui.TableNextColumn();
				if (UiHelper.IconButton(FontAwesomeIcon.Trash, $"Remove{i}"))
				{
					Service.Config.Notifications.Remove(notification);
				}
			}

			ImGui.EndTable();
		}

		/// <summary>
		/// Returns a message depending on if an audio file is found or not, and if it is not a supported format.
		/// Supported formats are mp3, wav, and ogg.
		/// </summary>
		/// <param name="path">Path to image</param>
		/// <returns>string</returns>
		private string FindSoundMessage(string path)
		{
			if (path.IsNullOrEmpty()) return "";

			bool fileFound = File.Exists(path);

			if (!fileFound) return "File not found.";

			string[] supportedImages = { ".wav", ".mp3" };

			bool isImage = supportedImages.Any(ext => Path.GetExtension(path).Trim() == ext);

			return isImage ? "" : "File is not supported. Use MP3, or WAV.";
		}

		private FileDialogManager SetupDialogManager()
		{
			var fileManager = new FileDialogManager { AddedWindowFlags = ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoDocking };

			// Remove Videos and Music.
			fileManager.CustomSideBarItems.Add(("Videos", string.Empty, 0, -1));

			return fileManager;
		}

		private bool IsPlaying(Notification notification)
		{
			return this.playingNotifications.ContainsKey(notification.Id);
		}

		private void PlayNotification(Notification notification)
		{
			CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
			this.playingNotifications.Add(notification.Id, cancellationTokenSource);
			SoundEngine.PlaySound(notification.AudioFilePath, notification.Volume, cancellationTokenSource.Token)
				.ContinueWith((t) =>
				{
					this.playingNotifications.Remove(notification.Id);
					cancellationTokenSource.Cancel();
				});
		}

		private void StopNotification(Notification notification)
		{
			if (this.playingNotifications.TryGetValue(notification.Id, out CancellationTokenSource cancellationTokenSource))
			{
				this.playingNotifications.Remove(notification.Id);
				cancellationTokenSource.Cancel();
			}
		}
	}
}
