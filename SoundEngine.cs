﻿using Dalamud.Utility;
using NAudio.Wave;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace UptimeHelper
{
    public static class SoundEngine
    {
        private static ConcurrentDictionary<Guid, WaveOutEvent> playingSounds = new ConcurrentDictionary<Guid, WaveOutEvent>();

        // Copied from PeepingTom plugin
        public static Task PlaySound(string path, float volume = 1.0f, CancellationToken cancellationToken = default)
        {
            if (path.IsNullOrEmpty() || !File.Exists(path)) return Task.CompletedTask;

            if (Process.GetCurrentProcess().Id != ProcessUtils.GetForegroundProcessId() && !Service.Config.PlayInBackground) return Task.CompletedTask;

			int soundDevice = Service.Config.SoundDeviceId;
            if (soundDevice < -1 || soundDevice > WaveOut.DeviceCount)
            {
                soundDevice = -1;
            }

            Guid playbackId = Guid.NewGuid();

            return Task.Run(() => {
                WaveStream reader;
                try
                {
                    reader = new AudioFileReader(path);
                }
                catch (Exception e)
                {
                    Dalamud.Logging.PluginLog.Error($"Could not play sound file: {e.Message}");
                    return;
                }

                using WaveChannel32 channel = new WaveChannel32(reader)
                {
                    Volume = volume,
                    PadWithZeroes = false,
                };

                using (reader)
                {
                    using WaveOutEvent output = new WaveOutEvent
                    {
                        DeviceNumber = soundDevice,
                    };
                    output.Init(channel);
                    output.Play();

                    playingSounds.TryAdd(playbackId, output);
                    
                    while (output.PlaybackState == PlaybackState.Playing && !cancellationToken.IsCancellationRequested)
                    {
                        Thread.Sleep(500);
                    }

                    output.Stop();
                    playingSounds.TryRemove(playbackId, out _);
                }
            }, cancellationToken);
        }

        public static void StopSound(Guid playbackId)
        {
            if (playingSounds.TryRemove(playbackId, out WaveOutEvent output))
            {
                output.Stop();
            }
        }

        public static void StopAllSounds()
        {
            Guid[] ids = playingSounds.Keys.ToArray();
            for (int i = 0; i < ids.Length; i++)
            {
                StopSound(ids[i]);
            }
        }
    }
}