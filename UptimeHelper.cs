﻿using System;
using System.Linq;
using Dalamud.Plugin;
using Dalamud.Logging;
using Dalamud.Game;
using Dalamud.Game.Gui;
using Dalamud.Game.ClientState;
using Dalamud.Game.Command;
using Dalamud.Game.ClientState.Objects;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Data;
using System.Collections.Generic;
using Dalamud.Interface.Windowing;
using UptimeHelper.Attributes;
using Dalamud.Game.ClientState.Statuses;

namespace UptimeHelper
{
	public class UptimeHelper : IDalamudPlugin {
        public string Name => "UptimeHelper";

        private readonly DalamudPluginInterface pluginInterface;
        private readonly PluginCommandManager<UptimeHelper> commandManager;
        private readonly WindowSystem windowSystem;

        private readonly Dictionary<string, ActiveStatus> activeStatuses;

        public UptimeHelper(
                DalamudPluginInterface pluginInterface,
                CommandManager commands,
                ChatGui chatGui,
                ClientState clientState,
                DataManager dataManager,
                Framework framework,
                TargetManager targets
            )
        {
            this.pluginInterface = pluginInterface;
            Service.ChatGui = chatGui;
            Service.ClientState = clientState;
            Service.DataManager = dataManager;
            Service.Framework = framework;
            Service.TargetManager = targets;

            try
            {
                Service.Config = (Configuration)pluginInterface.GetPluginConfig() ?? new Configuration();
            }
            catch
            {
                Service.Config = new Configuration();
            }
			Service.Config.Init(this, pluginInterface);

            // Initialize the UI
            this.windowSystem = new WindowSystem(typeof(UptimeHelper).AssemblyQualifiedName);

            var configWindow = this.pluginInterface.Create<PluginConfiguration>();
            if (configWindow is not null)
            {
                this.windowSystem.AddWindow(configWindow);
            }

            this.pluginInterface.UiBuilder.Draw += this.windowSystem.Draw;
            this.pluginInterface.UiBuilder.OpenConfigUi += this.ToggleConfigWindow;
            Service.Framework.Update += this.FrameworkOnUpdate;

            // Load all of our commands
            this.commandManager = new PluginCommandManager<UptimeHelper>(this, commands);

            this.activeStatuses = new Dictionary<string, ActiveStatus>();

            if (Service.Config.DebugChatText)
            {
                Service.ChatGui.Print($"UptimeHelper started!");
            }
        }

        public void InvalidateState()
        {
            this.activeStatuses.Clear();
        }

        private void FrameworkOnUpdate(Framework framework)
        {
            try
            {
                UpdateTargetStatus();
            }
            catch (Exception ex)
            {
                PluginLog.Error(ex.ToString());
            }
        }

        private void UpdateTargetStatus()
        {
            Dalamud.Game.ClientState.Objects.SubKinds.PlayerCharacter localPlayer = Service.ClientState.LocalPlayer;
            if (localPlayer is null) return;
            uint localPlayerId = localPlayer.ObjectId;

            // player statuses
            foreach (Status status in localPlayer.StatusList)
            {
				if (status.SourceId == localPlayerId)
                {
					this.AddNewStatus(localPlayer, status);
				}
            }

            // target statuses, but not if player is targeting self
            if (Service.TargetManager.Target is BattleChara target && target.ObjectId != localPlayerId)
            {
                foreach (Status status in target.StatusList)
                {
                    if (status.SourceId == localPlayerId)
                    {
						this.AddNewStatus(target, status);
					}
                }
            }

			this.NotifyExpiringStatuses();
		}

        private void AddNewStatus(BattleChara target, Status status)
		{
            if (Service.Config.NotificationLookup.TryGetValue(status.StatusId, out Dictionary<Guid, Notification> notifications))
            {
                foreach (Notification notification in notifications.Values)
                {
                    float offsetSeconds = notification.OffsetSeconds;
                    if (!notification.Enabled) continue;
                    AddNewStatus(target, status, offsetSeconds, notification.Id);
                }
            }
            else
            {
                AddNewStatus(target, status, 0.0f, Guid.Empty);
            }
		}

		private void AddNewStatus(BattleChara target, Status status, float offsetSeconds, Guid notificationId)
		{
			float notificationDelay = status.RemainingTime + offsetSeconds;
			DateTime expirationDate = DateTime.UtcNow.AddSeconds(notificationDelay);

			ActiveStatus activeStatus = new ActiveStatus
			{
                NotificationId = notificationId,
				StatusId = status.StatusId,
				Status = status,
				ExpirationDate = expirationDate,
				Target = target,
			};

			if (!this.NotifyCondition(activeStatus))
			{
				string key = $"{target.ObjectId}:{activeStatus.NotificationId}";

				if (!this.activeStatuses.ContainsKey(key))
				{
					if (Service.Config.DebugChatText)
					{
						Service.ChatGui.Print($"{status.StatusId} {status.GameData.Name} status added! {notificationDelay:N}s");
					}

					// only set name once, since accessing GameData is slow
					activeStatus.Name = status.GameData.Name;
					this.activeStatuses[key] = activeStatus;
				}

				this.activeStatuses[key].ExpirationDate = expirationDate;
			}
		}

		private void NotifyExpiringStatuses()
        {
            string[] keys = this.activeStatuses.Keys.ToArray();
            foreach (string key in keys)
            {
				ActiveStatus activeStatus = this.activeStatuses[key];
				bool targetAlive = (activeStatus.Target?.CurrentHp ?? 0) > 0;
				if (this.NotifyCondition(activeStatus) || !targetAlive)
                {
                    if (!targetAlive && Service.Config.DebugChatText)
                    {
                        Service.ChatGui.Print($"{activeStatus.Name} {activeStatus.Target?.Name} target died!");
                    }

                    if (true
                        && targetAlive
                        && Service.Config.NotificationLookup.TryGetValue(activeStatus.StatusId, out Dictionary<Guid, Notification> notifications)
                        && notifications.TryGetValue(activeStatus.NotificationId, out Notification notification)
                    ) {
                        // notify user
                        SoundEngine.PlaySound(notification.AudioFilePath, notification.Volume);
                        if (notification.ShowInChat)
                        {
                            Service.ChatGui.Print($"{activeStatus.Name} ready!");
                        }
                    }

                    // remove from cache
                    this.activeStatuses.Remove(key);
                }
			}
		}

        private bool NotifyCondition(ActiveStatus activeStatus)
        {
            return DateTime.UtcNow > activeStatus.ExpirationDate;
        }

        [Command("/uptimehelper")]
        [Aliases("/uh")]
        [HelpMessage("Configuration Window Toggle")]
        public void ConfigCommand(string command, string args)
        {
            ToggleConfigWindow();
        }

        private void ToggleConfigWindow()
        {
            var configWindow = this.windowSystem.GetWindow(PluginConfiguration.WINDOW_NAME);
            configWindow.IsOpen ^= true;
        }


        #region IDisposable Support
        public void Dispose(bool disposing)
        {
            if (!disposing) return;

            SoundEngine.StopAllSounds();

            this.commandManager.Dispose();

            Service.Config.Save();

            this.pluginInterface.UiBuilder.Draw -= this.windowSystem.Draw;
            this.pluginInterface.UiBuilder.OpenConfigUi -= this.ToggleConfigWindow;
            Service.Framework.Update -= FrameworkOnUpdate;

            this.windowSystem.RemoveAllWindows();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
