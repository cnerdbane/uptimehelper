﻿using Dalamud.Data;
using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Objects;
using Dalamud.Game.Gui;

namespace UptimeHelper
{
    internal static class Service
    {
        internal static ClientState ClientState;
        internal static Framework Framework;
        internal static Configuration Config;
        internal static TargetManager TargetManager;
		internal static ChatGui ChatGui;
		internal static DataManager DataManager;
	}
}