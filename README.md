# Uptime Helper

## What is it?

Uptime Helper is a Dalamud plugin for FFXIV which gives audio notifications to remind you to use your abilities.

## Install

Add this to your repository list
`https://gitlab.com/cnerdbane/uptimehelper/-/raw/main/repo.json`

## How to use

`/uptimehelper' brings up the configuration window.

![config](images/image1.png)

Search for the status effect you want to monitor, then choose it from the drop down list and click `Add`.

If there are multiple with the same name, you may need to enable "Debug Chat Text" and use the ability and check which number ID is detected, then choose that ID from the drop down list.

Using the `Browse` button on the new row, select your `.wav` or `.mp3` file.  The `Play` button will test play your notification sound at the volume selected.

The offset column is how long in seconds the notification should happen before or after the status effect ends. For example, `-3` means the notification will go off 3 seconds before the effect ends (good for abilities like `Dia`), and `35` means it will go off 35 seconds AFTER the effect ends (good for abilities like `Lucid Dreaming` to match the 60s cooldown).

The chat checkbox enables additional chat message notifications.
