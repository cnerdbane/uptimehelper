﻿
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UptimeHelper
{
	public class Notification
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public uint StatusId { get; set; }
		public string AudioFilePath { get; set; } = String.Empty;
		public float Volume { get; set; } = 1.0f;
		public float OffsetSeconds { get; set; } = -3.0f;
		public bool ShowInChat { get; set; } = true;
		public bool Enabled { get; set; } = true;
	}
}
