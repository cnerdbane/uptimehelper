﻿using System;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Game.ClientState.Statuses;
using Lumina.Text;

namespace UptimeHelper
{
	public class ActiveStatus
	{
		public Status Status { get; internal set; }
		public DateTime ExpirationDate { get; internal set; }
		public uint StatusId { get; internal set; }
		public BattleChara Target { get; internal set; }
		public string Name { get; internal set; }
		public Guid NotificationId { get; internal set; }
	}
}
